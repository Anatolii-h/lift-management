﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lift_Management.Dto;
using Lift_Management.Model;

namespace Lift_Management.Service
{
    interface IFindWorkerService
    {
        WorkerAccountDto FindFreeWorkerByDistrict(string district);
        WorkerAccountDto FindWorkerByName(string firstName, string lastName);
        WorkerAccountDto FindWorkerById(Guid id);
    }
}
