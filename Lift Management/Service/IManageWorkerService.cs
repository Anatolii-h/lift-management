﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lift_Management.Model;

namespace Lift_Management.Service
{
    interface IManageWorkerService
    {
        void AssignOrderToWorker(Guid workerId, Guid orderId);
        void CompleteOrder(Guid workerId);
    }
}
