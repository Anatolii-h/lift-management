﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lift_Management.Model;
using Lift_Management.Utils.Validators;

namespace Lift_Management.Service
{
    interface IChangeSalaryService
    {
        void ChangeAdministratorsSalary([SalaryValidator] int newSalary);
        void ChangeDispetchersSalary([SalaryValidator] int newSalary);
        void ChangeWorkersSalary(Guid Id, [SalaryValidator] int newSalary);
    }
}
