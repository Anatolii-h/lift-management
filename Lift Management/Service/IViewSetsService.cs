﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lift_Management.Model;

namespace Lift_Management.Service
{
    interface IViewSetsService
    {
        List<Order> ViewOrders();
        List<WorkerAccount> ViewWorkers();
        List<DispetcherAccount> ViewDispetchers();
        List<AdministratorAccount> ViewAdministrators();
    }
}
