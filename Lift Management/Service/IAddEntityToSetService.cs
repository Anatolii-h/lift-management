﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lift_Management.Model;

namespace Lift_Management.Service
{
    interface IAddEntityToSetService
    {
        void AddOrder(Order order);
        void AddWorker(WorkerAccount worker);
        void AddDispetcher(DispetcherAccount dispetcher);
        void AddAdministrator(AdministratorAccount administrator);
    }
}
