﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Lift_Management.Model;
using System.Diagnostics;

namespace Lift_Management.Repository.EntityFramework
{
    class LiftDataContext : DbContext
    {
        public LiftDataContext() : base("Lift_Management.Properties.Settings.testdbConnectionString")
        {
            /*Database.Log = (e) => { Debug.WriteLine(e);};*/
        }

        public DbSet<Account> Accounts { get; set; }
        public DbSet<WorkerAccount> Workers { get; set; }
        public DbSet<DispetcherAccount> Dispetchers { get; set; }
        public DbSet<AdministratorAccount> Administrators { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Contacts> Contacts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("Accounts");
            });
            modelBuilder.Entity<AdministratorAccount>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("Administrators");
            });
            modelBuilder.Entity<DispetcherAccount>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("Dispetchers");
            });
            modelBuilder.Entity<WorkerAccount>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("Workers");
            });
        }
    }
}
