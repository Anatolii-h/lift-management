﻿using System.Reflection;

namespace Lift_Management.Repository.EntityFramework
{
    static class RepositoryAssemblyInformer
    {
        public static AssemblyName assemblyName { get; set; }

        static RepositoryAssemblyInformer()
        {
            assemblyName = Assembly.GetExecutingAssembly().GetName();
        }
    }
}
