﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lift_Management.Model;

namespace Lift_Management.Repository.EntityFramework
{
    class AddressRepository : BasicRepository<Address>, IAddressRepository

    {
        public AddressRepository(LiftDataContext dbContext)
            : base(dbContext, dbContext.Addresses)
        { }

        public Address GetAddressById(Guid id)
        {
            return GetDBSet().Where(a => a.Id == id).SingleOrDefault();
        }
    }
}
