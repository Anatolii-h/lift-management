﻿namespace Lift_Management.Repository.EntityFramework
{
    static class RepositoryFactory
    {
        public static IAddressRepository MakeAddressRepository(LiftDataContext dbContext)
        {
            return new AddressRepository(dbContext);
        }

        public static IAdministratorAccountRepository MakeAdministratorAccountRepository(LiftDataContext dbContext)
        {
            return new AdministratorAccountRepository(dbContext);
        }

        public static IContactsRepository MakeContactsRepository(LiftDataContext dbContext)
        {
            return new ContactsRepository(dbContext);
        }

        public static IDispetcherAccountRepository MakeDispetcherAccountRepository(LiftDataContext dbContext)
        {
            return new DispetcherAccountRepository(dbContext);
        }

        public static IOrderRepository MakeOrderRepository(LiftDataContext dbContext)
        {
            return new OrderRepository(dbContext);
        }

        public static IWorkerAccountRepository MakeWorkerAccountRepository(LiftDataContext dbContext)
        {
            return new WorkerAccountRepository(dbContext);
        }
    }
}
