﻿using Lift_Management.Repository;
using Lift_Management.Repository.EntityFramework;
using Lift_Management.Service;
using Lift_Management.Service.Impl;

namespace Lift_Management
{
    class ServiceProvider
    {
        private LiftDataContext dbContext;

        public ServiceProvider(LiftDataContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public IAddEntityToSetService ProvideAddEntityToSetService()
        {
            IAdministratorAccountRepository administratorAccountRepository = RepositoryFactory.MakeAdministratorAccountRepository(dbContext);
            IDispetcherAccountRepository dispetcherAccountRepository = RepositoryFactory.MakeDispetcherAccountRepository(dbContext);
            IWorkerAccountRepository workerAccountRepository = RepositoryFactory.MakeWorkerAccountRepository(dbContext);
            IOrderRepository orderRepository = RepositoryFactory.MakeOrderRepository(dbContext);

            return ServiceFactory.MakeAddEntityToSetService(administratorAccountRepository
                , dispetcherAccountRepository, workerAccountRepository, orderRepository);
        }

        public IChangeSalaryService ProvideChangeSalaryService()
        {
            IAdministratorAccountRepository administratorAccountRepository = RepositoryFactory.MakeAdministratorAccountRepository(dbContext);
            IDispetcherAccountRepository dispetcherAccountRepository = RepositoryFactory.MakeDispetcherAccountRepository(dbContext);
            IWorkerAccountRepository workerAccountRepository = RepositoryFactory.MakeWorkerAccountRepository(dbContext);

            return ServiceFactory.MakeChangeSalaryService(administratorAccountRepository, dispetcherAccountRepository, workerAccountRepository);
        }

        public IFindWorkerService ProvideFindWorkerService()
        {
            IWorkerAccountRepository workerAccountRepository = RepositoryFactory.MakeWorkerAccountRepository(dbContext);

            return ServiceFactory.MakeFindWorkerService(workerAccountRepository);
        }

        public IManageWorkerService ProvideManageWorkerService()
        {
            IWorkerAccountRepository workerAccountRepository = RepositoryFactory.MakeWorkerAccountRepository(dbContext);
            IOrderRepository orderRepository = RepositoryFactory.MakeOrderRepository(dbContext);

            return ServiceFactory.MakeManageWorkerService(workerAccountRepository, orderRepository);
        }

        public IViewSetsService ProvideViewSetsService()
        {
            IAdministratorAccountRepository administratorAccountRepository = RepositoryFactory.MakeAdministratorAccountRepository(dbContext);
            IDispetcherAccountRepository dispetcherAccountRepository = RepositoryFactory.MakeDispetcherAccountRepository(dbContext);
            IWorkerAccountRepository workerAccountRepository = RepositoryFactory.MakeWorkerAccountRepository(dbContext);
            IOrderRepository orderRepository = RepositoryFactory.MakeOrderRepository(dbContext);

            return ServiceFactory.MakeViewSetsService(administratorAccountRepository
                , dispetcherAccountRepository, workerAccountRepository, orderRepository);
        }
    }
}
