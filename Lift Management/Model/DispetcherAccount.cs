﻿namespace Lift_Management.Model
{
    class DispetcherAccount : Account
    {
        public int Salary { get; set; }

        public DispetcherAccount()
        { }

        public DispetcherAccount(string firstName, string lastName, Contacts contact, string password, int salary) : base(firstName, lastName, contact, password)
        {
            Salary = salary;
        }

        /*public WorkerAccount FindWorkerByName(string firstName, string lastName)
        {
            return DataProvider.FindWorkerByName(firstName, lastName);
        }

        public WorkerAccount[] ViewWorkers()
        {
            return DataProvider.WatchAllWorkers();
        }

        public Order[] ViewOrders()
        {
            return DataProvider.WatchAllOrders();
        }

        public void AddWorker(WorkerAccount worker)
        {
            DataProvider.AddWorker(worker);
        }

        public void AddOrder(Order order)
        {
            DataProvider.AddOrder(order);
        }*/

        public override string ToString()
        {
            return "ACCOUNT TYPE: Dispetcher" +
                "\r\nSALARY: " + Salary +
                "\r\n" + "ID: " + Id +
                "\r\nFIRST NAME: " + FirstName +
                "\r\nLAST NAME: " + LastName +
                "\r\nPASSWORD: " + Password +
                "\r\nCONTACTS:\r\n" + Contact.ToString();
        }

        /*public WorkerAccount FindFreeWorkerByDistrict(string district)
        {
            return DataProvider.FindFreeWorker(district);
        }

        public void AssignOrderToWorker(WorkerAccount worker, Order order)
        {
            DataProvider.AssignOrderToWorker(worker, order);
        }

        public void UpdateWorkerStatus(WorkerAccount worker, Order order, WorkerStatus newStatus)
        {
            DataProvider.UpdateWorkerStatus(worker, order, newStatus);
        }*/
    }
}
