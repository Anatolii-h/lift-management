﻿namespace Lift_Management.Model
{
    class AdministratorAccount : Account
    {
        public int Salary { get; set; }

        public AdministratorAccount()
        { }

        public AdministratorAccount(string firstName, string lastName, Contacts contact, string password, int salary) : base(firstName, lastName, contact, password)
        {
            Salary = salary;
        }

        /*public void ChangeWorkerSalary(WorkerAccount person, int newSalary)
        {
            person.Salary = newSalary;
        }

        public void ChangeDispetcherSalary(DispetcherAccount person, int newSalary)
        {
            person.Salary = newSalary;
        }

        public WorkerAccount[] ViewWorkers()
        {
            return DataProvider.WatchAllWorkers();
        }

        public Order[] ViewOrders()
        {
            return DataProvider.WatchAllOrders();
        }

        public void AddWorker(WorkerAccount worker)
        {
            DataProvider.AddWorker(worker);
        }

        public void AddOrder(Order order)
        {
            DataProvider.AddOrder(order);
        }*/

        public override string ToString()
        {
            return "ACCOUNT TYPE: Administrator" +
                "\r\nSALARY: " + Salary +
                "\r\n" + "ID: " + Id +
                "\r\nFIRST NAME: " + FirstName +
                "\r\nLAST NAME: " + LastName +
                "\r\nPASSWORD: " + Password +
                "\r\nCONTACTS:\r\n" + Contact.ToString();
        }
    }
}
