﻿using Lift_Management.Dependencies;
using Lift_Management.Repository.EntityFramework;
using Microsoft.Practices.Unity;
using System;
using System.IO;

namespace Lift_Management
{
    class Program
    {
        static void Main(string[] args)
        {
            TestByUnityContainer();

            Console.WriteLine("Test program completed");
            Console.Read();
        }

        public static void TestByServiceProvider()
        {
            using (LiftDataContext dbContext = new LiftDataContext())
            {
                ServiceProvider serviceProvider = new ServiceProvider(dbContext);

                Generators_Reporters.ThroughProvider.TestModelGenerator testModelGenerator = new Generators_Reporters.ThroughProvider.TestModelGenerator(serviceProvider);

                testModelGenerator.GenerateTest();
            }

            using (LiftDataContext dbContext = new LiftDataContext())
            {
                ServiceProvider serviceProvider = new ServiceProvider(dbContext);

                Generators_Reporters.ThroughProvider.TestModelReporter testModelReporter = new Generators_Reporters.ThroughProvider.TestModelReporter(serviceProvider);

                string report = testModelReporter.GenerateReport();

                File.AppendAllText(Directory.GetCurrentDirectory() + "\\Reports.txt", report);

                Console.WriteLine(report);
            }
        }

        public static void TestByUnityContainer()
        {
            /*try
            {*/
                using (var dbContext = new LiftDataContext())
                using (var unityContainer = new UnityContainer())
                {
                    dbContext.Database.Initialize(true);

                    ContainerBoostraper.RegisterTypes(unityContainer, dbContext);

                    Generators_Reporters.ThroghUnityContainer.TestModelGenerator generator = new Generators_Reporters.ThroghUnityContainer.TestModelGenerator(unityContainer);
                    generator.GenerateTest();
                }

                using (var dbContext = new LiftDataContext())
                using (var unityContainer = new UnityContainer())
                {
                    ContainerBoostraper.RegisterTypes(unityContainer, dbContext);

                    Generators_Reporters.ThroghUnityContainer.TestModelReporter reportGenerator = new Generators_Reporters.ThroghUnityContainer.TestModelReporter(unityContainer, Console.Out);
                    reportGenerator.GenerateReport();
                }
            /*}
            catch (Exception e)
            {
                while (e != null)
                {
                    Console.WriteLine("{0}: {1}", e.GetType().FullName, e.Message);

                    e = e.InnerException;
                }
            }*/
        }
    }
}
