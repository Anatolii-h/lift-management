﻿/*using System;
using System.Collections.Generic;
using System.Linq;
using Lift_Management.Exceptions;
using Lift_Management.Model;
using Lift_Management.EntityFramework;

namespace Lift_Management
{
    static class DataProvider
    {
        public static WorkerAccount FindFreeWorker(string district)
        {
            using (LiftDataContext db = new LiftDataContext())
            {
                if (db.Workers.Count() > 0)
                {
                    var workers = db.Workers.ToArray();

                    bool noWorkersInCurrentDistrict = true;

                    for (int i = 0; i < workers.Length; i++)
                    {
                        db.Entry(workers[i]).Collection("CompletedOrders").Load();
                        db.Entry(workers[i]).Reference("CurrentOrder").Load();

                        if (workers[i].CurrentOrder != null)
                            db.Entry(workers[i].CurrentOrder).Reference("address").Load();

                        db.Entry(workers[i]).Reference("Contact").Load();
                        db.Entry(workers[i].Contact).Reference("address").Load();

                        if (workers[i].Status == WorkerStatus.Busy && workers[i].Contact.address.District != district)
                            continue;
                        else if (workers[i].Status == WorkerStatus.Busy && workers[i].Contact.address.District == district)
                            noWorkersInCurrentDistrict = false;
                        else if (workers[i].Status == WorkerStatus.Free && workers[i].Contact.address.District != district)
                            continue;
                        else
                        {
                            return workers[i];
                        }
                    }

                    if (noWorkersInCurrentDistrict)
                        throw new WorkingProcessException("Organization has no workers in this district");
                    else
                        throw new WorkingProcessException("All workers are busy");
                }
                else throw new DbSetIsEmptyException(typeof(WorkerAccount), "Worker list is empty");
            }
        }

        public static WorkerAccount FindWorkerByName(string firstName, string lastName)
        {
            using (LiftDataContext db = new LiftDataContext())
            {
                if (db.Workers.Count() > 0)
                {
                    var workers = db.Workers.ToList();

                    for (int i = 0; i < db.Workers.Count(); i++)
                    {
                        if (workers[i].FirstName == firstName && workers[i].LastName == lastName)
                        {
                            db.Entry(workers[i]).Collection("CompletedOrders").Load();
                            db.Entry(workers[i]).Reference("CurrentOrder").Load();

                            if (workers[i].CurrentOrder != null)
                                db.Entry(workers[i].CurrentOrder).Reference("address").Load();

                            db.Entry(workers[i]).Reference("Contact").Load();
                            db.Entry(workers[i].Contact).Reference("address").Load();
                            return workers[i];
                        }
                        else
                            continue;
                    }
                    throw new EntityNotFoundException(firstName+lastName, typeof(WorkerAccount), "Worker with this first name+last name does not exist");
                }
                else throw new DbSetIsEmptyException(typeof(WorkerAccount), "Worker list is empty");
            }
        }

        public static void UpdateWorkerStatus(WorkerAccount worker, Order order, WorkerStatus newStatus)
        {
            using (LiftDataContext db = new LiftDataContext())
            {
                Order currentOrder = order == null? null : db.Orders.Find(order.Id);
                WorkerAccount currentWorker = db.Workers.Find(worker.Id);

                db.Entry(currentWorker).Collection("CompletedOrders").Load();
                db.Entry(currentWorker).Reference("CurrentOrder").Load();

                if (currentWorker.CurrentOrder != null)
                    db.Entry(currentWorker.CurrentOrder).Reference("address").Load();

                db.Entry(currentWorker).Reference("Contact").Load();
                db.Entry(currentWorker.Contact).Reference("address").Load();

                if (currentOrder != null)
                    db.Entry(currentOrder).Reference("address").Load();

                if (newStatus == WorkerStatus.Busy)
                {
                    currentWorker.CurrentOrder = currentOrder;
                    currentWorker.CurrentOrderId = currentOrder.Id;
                }
                else
                {
                    currentWorker.CompletedOrders.Add(currentWorker.CurrentOrder);

                    currentOrder = currentWorker.CurrentOrder;

                    currentWorker.CurrentOrder = null;
                    currentWorker.CurrentOrderId = null;
                    currentOrder.Status = OrderStatus.Completed;
                }

                currentWorker.Status = newStatus;

                db.SaveChanges();
            }
        }

        public static void AssignOrderToWorker(WorkerAccount worker, Order order)
        {
            using (LiftDataContext db = new LiftDataContext())
            {
                db.Workers.Find(worker).AssignOrder(order);
                db.SaveChanges();
            }
        }

        public static void AddWorker(WorkerAccount worker)
        {
            using (LiftDataContext db = new LiftDataContext())
            {
                db.Addresses.Add(worker.Contact.address);
                db.Contacts.Add(worker.Contact);
                db.Workers.Add(worker);
                db.SaveChanges();
            }
        }

        public static void AddAdministrator(AdministratorAccount administrator)
        {
            using (LiftDataContext db = new LiftDataContext())
            {
                if (db.Administrators.Count() < 1)
                {
                    db.Addresses.Add(administrator.Contact.address);
                    db.Contacts.Add(administrator.Contact);
                    db.Administrators.Add(administrator);
                    db.SaveChanges();
                }
            }
        }

        public static void AddDispetcher(DispetcherAccount dispetcher)
        {
            using (LiftDataContext db = new LiftDataContext())
            {
                if (db.Dispetchers.Count() < 1)
                {
                    db.Addresses.Add(dispetcher.Contact.address);
                    db.Contacts.Add(dispetcher.Contact);
                    db.Dispetchers.Add(dispetcher);
                    db.SaveChanges();
                }
            }
        }

        public static void AddOrder(Order order)
        {
            using (LiftDataContext db = new LiftDataContext())
            {
                db.Addresses.Add(order.address);
                db.Orders.Add(order);
                db.SaveChanges();
            }
        }

        public static Order[] WatchAllOrders()
        {
            using (LiftDataContext db = new LiftDataContext())
            {
                if (db.Orders.Count() > 0)
                {
                    var orders = db.Orders.ToArray();

                    for (int i = 0; i < orders.Length; i++)
                        db.Entry(orders[i]).Reference("address").Load();

                    return orders;
                }
                else throw new DbSetIsEmptyException(typeof(Order), "Order list is empty");
            }
        }

        public static WorkerAccount[] WatchAllWorkers()
        {
            using (LiftDataContext db = new LiftDataContext())
            {
                if (db.Workers.Count() > 0)
                {
                    var workers = db.Workers.ToArray();

                    for (int i = 0; i < workers.Length; i++)
                    {
                        db.Entry(workers[i]).Collection("CompletedOrders").Load();
                        db.Entry(workers[i]).Reference("CurrentOrder").Load();

                        if (workers[i].CurrentOrder != null)
                            db.Entry(workers[i].CurrentOrder).Reference("address").Load();

                        db.Entry(workers[i]).Reference("Contact").Load();
                        db.Entry(workers[i].Contact).Reference("address").Load();
                    }
                    return workers;
                }
                else throw new DbSetIsEmptyException(typeof(WorkerAccount), "Worker list is empty");
            }
        }
    }
}
*/