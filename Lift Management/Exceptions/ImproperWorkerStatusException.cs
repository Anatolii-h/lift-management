﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lift_Management.Model;

namespace Lift_Management.Exceptions
{
    class ImproperWorkerStatusException : DomainException
    {
        public WorkerStatus CurrentStatus { get; private set; }
 
        public ImproperWorkerStatusException(WorkerStatus currentStatus, string message) : base(message)
        {
            CurrentStatus = currentStatus;
        }
    }
}
