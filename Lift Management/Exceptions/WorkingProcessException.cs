﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lift_Management.Exceptions
{
    class WorkingProcessException : DomainException
    {
        public WorkingProcessException(string message) : base(message)
        {}
    }
}
