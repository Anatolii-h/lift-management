﻿using Lift_Management.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lift_Management.Dto
{
    class AdministratorAccountDto : AccountDto
    {
        public AdministratorAccountDto(Guid id, string fname, string lname, Contacts contacts) : base(id, fname, lname, contacts)
        { }
    }
}
