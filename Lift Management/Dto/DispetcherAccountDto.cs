﻿using Lift_Management.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lift_Management.Dto
{
    class DispetcherAccountDto : AccountDto
    {
        public DispetcherAccountDto(Guid id, string fname, string lname, Contacts contacts) : base(id, fname, lname, contacts)
        { }
    }
}
