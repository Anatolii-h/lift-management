﻿using System;
using System.Diagnostics.Tracing;

using Microsoft.Practices.EnterpriseLibrary.SemanticLogging;
using Microsoft.Practices.Unity;

namespace Lift_Management.Dependencies
{
    class LogListener : IDisposable
    {
        [Dependency]
        protected LiftEventSource Log { get; set; }

        private EventListener listenerInfo;
        private EventListener listenerErrors;

        internal void OnStartup()
        {
            listenerInfo = FlatFileLog.CreateListener("pizzario_services.log");
            listenerInfo.EnableEvents(Log, EventLevel.LogAlways, LiftEventSource.Keywords.ServiceTracing);

            listenerErrors = FlatFileLog.CreateListener("pizzario_diagnostic.log");
            listenerErrors.EnableEvents(Log, EventLevel.LogAlways, LiftEventSource.Keywords.Diagnostic);

            Log.StartupSucceeded();
        }

        public void Dispose()
        {
            listenerInfo.DisableEvents(Log);
            listenerErrors.DisableEvents(Log);

            listenerInfo.Dispose();
            listenerErrors.Dispose();
        }
    }
}
