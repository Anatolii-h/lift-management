﻿using Lift_Management.Repository;

namespace Lift_Management.Service.Impl
{
    static class ServiceFactory
    {
        public static IAddEntityToSetService MakeAddEntityToSetService(IAdministratorAccountRepository administratorAccountRepository
            , IDispetcherAccountRepository dispetcherAccountRepository
            , IWorkerAccountRepository workerAccountRepository
            , IOrderRepository orderRepository)
        {
            return new AddEntityToSetService(administratorAccountRepository, dispetcherAccountRepository, workerAccountRepository, orderRepository);
        }

        public static IChangeSalaryService MakeChangeSalaryService(IAdministratorAccountRepository administratorAccountRepository
            , IDispetcherAccountRepository dispetcherAccountRepository
            , IWorkerAccountRepository workerAccountRepository)
        {
            return new ChangeSalaryService(administratorAccountRepository, dispetcherAccountRepository, workerAccountRepository);
        }

        public static IFindWorkerService MakeFindWorkerService(IWorkerAccountRepository workerAccountRepository)
        {
            return new FindWorkerService(workerAccountRepository);
        }

        public static IManageWorkerService MakeManageWorkerService(IWorkerAccountRepository workerAccountRepository
            , IOrderRepository orderRepository)
        {
            return new ManageWorkerService(workerAccountRepository, orderRepository);
        }

        public static IViewSetsService MakeViewSetsService(IAdministratorAccountRepository administratorAccountRepository
            , IDispetcherAccountRepository dispetcherAccountRepository
            , IWorkerAccountRepository workerAccountRepository
            , IOrderRepository orderRepository)
        {
            return new ViewSetsService(administratorAccountRepository, dispetcherAccountRepository, workerAccountRepository, orderRepository);
        }
    }
}
