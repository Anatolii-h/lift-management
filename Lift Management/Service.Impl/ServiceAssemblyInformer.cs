﻿using System.Reflection;

namespace Lift_Management.Service.Impl
{
    static class ServiceAssemblyInformer
    {
        public static AssemblyName assemblyName { get; set; }

        static ServiceAssemblyInformer()
        {
            assemblyName = Assembly.GetExecutingAssembly().GetName();
        }
    }
}
