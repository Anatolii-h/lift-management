﻿using Lift_Management.Model;
using Lift_Management.Service;
using Microsoft.Practices.Unity;
using System;

namespace Lift_Management.Generators_Reporters.ThroghUnityContainer
{
    class TestModelGenerator
    {
        private IUnityContainer unityContainer;

        public TestModelGenerator(IUnityContainer unityContainer)
        {
            this.unityContainer = unityContainer;
        }

        public void GenerateTest()
        {
            GenerateEntities();
            GenerateOrder();
            AssignOrder();
            CompleteOrder();
        }

        private void GenerateEntities()
        {
            #region Model creation
            Contacts dispetcherContacts = new Contacts("0503213454", "sb@mail.su", new Address("12", "Komsomolskaja", "15", 3, 14));
            DispetcherAccount dispetcherAccount = new DispetcherAccount("Sergei", "Belov", dispetcherContacts, "12345", 1000);

            Contacts administratorContacts = new Contacts("0509478274", "ji@mail.su", new Address("3", "Bankovaja", "5", null));
            AdministratorAccount administratorAccount = new AdministratorAccount("Julii", "Ivanov", administratorContacts, "54321", 500);

            Contacts workerContacts = new Contacts("0506390641", "ik@mail.su", new Address("5", "Komsomolskaya", "1", 1, 5));
            WorkerAccount workerAccount = new WorkerAccount("Ivan", "Komar", workerContacts, "000111", 5000);
            #endregion

            //---

            IAddEntityToSetService service = unityContainer.Resolve<IAddEntityToSetService>();

            //---

            service.AddAdministrator(administratorAccount);
            service.AddDispetcher(dispetcherAccount);
            service.AddWorker(workerAccount);
        }

        private void GenerateOrder()
        {
            Order order = new Order(new Address("8", "Druzhinnikov", "7A", 4), "Doors do not open", DateTime.Now);

            //---

            IAddEntityToSetService service = unityContainer.Resolve<IAddEntityToSetService>();

            //---

            service.AddOrder(order);
        }

        private void AssignOrder()
        {
            IFindWorkerService finder_service = unityContainer.Resolve<IFindWorkerService>();
            IManageWorkerService managing_service = unityContainer.Resolve<IManageWorkerService>();
            IViewSetsService viewer_service = unityContainer.Resolve<IViewSetsService>();

            //---

            var worker = finder_service.FindWorkerByName("Ivan", "Komar");
            var order = viewer_service.ViewOrders()[0];

            managing_service.AssignOrderToWorker(worker.Id, order.Id);
        }

        private void CompleteOrder()
        {
            IFindWorkerService finder_service = unityContainer.Resolve<IFindWorkerService>();
            IManageWorkerService managing_service = unityContainer.Resolve<IManageWorkerService>();

            //---

            var worker = finder_service.FindWorkerByName("Ivan", "Komar");

            managing_service.CompleteOrder(worker.Id);
        }
    }
}
