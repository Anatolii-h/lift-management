﻿using Lift_Management.Service;
using Microsoft.Practices.Unity;
using System;
using System.IO;

namespace Lift_Management.Generators_Reporters.ThroghUnityContainer
{
    class TestModelReporter
    {
        private IUnityContainer unityContainer;
        private TextWriter output;

        public TestModelReporter(IUnityContainer unityContainer, TextWriter output)
        {
            this.unityContainer = unityContainer;
            this.output = output;
        }

        public void GenerateReport()
        {
            string report = "Report [" + DateTime.Now.ToString() + "]\n\n";

            output.WriteLine(report);
            output.WriteLine();
            output.WriteLine("==== Administrator ===");
            output.WriteLine(ReportAdministrator());
            output.WriteLine();
            output.WriteLine("==== Dispetcher ===");
            output.WriteLine(ReportDispetcher());
            output.WriteLine();
            output.WriteLine("==== Worker ===");
            output.WriteLine(ReportWorker());
            output.WriteLine();
            output.WriteLine("==== Order ===");
            output.WriteLine(ReportOrder());
        }

        private string ReportOrder()
        {
            IViewSetsService viewer_service = unityContainer.Resolve<IViewSetsService>();

            //---

            var order = viewer_service.ViewOrders()[0];

            //---

            return order.ToString();
        }

        private string ReportWorker()
        {
            IViewSetsService viewer_service = unityContainer.Resolve<IViewSetsService>();

            //---

            var worker = viewer_service.ViewWorkers()[0];

            //---

            return worker.ToString();
        }

        private string ReportDispetcher()
        {
            IViewSetsService viewer_service = unityContainer.Resolve<IViewSetsService>();

            //---

            var dispetcher = viewer_service.ViewDispetchers()[0];

            //---

            return dispetcher.ToString();
        }

        private string ReportAdministrator()
        {
            IViewSetsService viewer_service = unityContainer.Resolve<IViewSetsService>();

            //---

            var administrator = viewer_service.ViewAdministrators()[0];

            //---

            return administrator.ToString();
        }
    }
}
