﻿using Lift_Management.Service;
using System;

namespace Lift_Management.Generators_Reporters.ThroughProvider
{
    class TestModelReporter
    {
        private ServiceProvider serviceProvider;

        public TestModelReporter(ServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public string GenerateReport()
        {
            string report = "Report [" + DateTime.Now.ToString() + "]\n\n";

            report += ReportAdministrator() + "\n\n";
            report += ReportDispetcher() + "\n\n";
            report += ReportWorker() + "\n\n";
            report += ReportOrder() + "\n";

            return report;
        }

        private string ReportOrder()
        {
            IViewSetsService viewer_service = serviceProvider.ProvideViewSetsService();

            //---

            var order = viewer_service.ViewOrders()[0];

            //---

            return order.ToString();
        }

        private string ReportWorker()
        {
            IViewSetsService viewer_service = serviceProvider.ProvideViewSetsService();

            //---

            var worker = viewer_service.ViewWorkers()[0];

            //---

            return worker.ToString();
        }

        private string ReportDispetcher()
        {
            IViewSetsService viewer_service = serviceProvider.ProvideViewSetsService();

            //---

            var dispetcher = viewer_service.ViewDispetchers()[0];

            //---

            return dispetcher.ToString();
        }

        private string ReportAdministrator()
        {
            IViewSetsService viewer_service = serviceProvider.ProvideViewSetsService();

            //---

            var administrator = viewer_service.ViewAdministrators()[0];

            //---

            return administrator.ToString();
        }
    }
}
