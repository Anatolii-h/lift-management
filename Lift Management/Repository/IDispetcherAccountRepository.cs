﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lift_Management.Model;

namespace Lift_Management.Repository
{
    interface IDispetcherAccountRepository : IRepository<DispetcherAccount>
    {
        DispetcherAccount GetDispetcher();
    }
}
