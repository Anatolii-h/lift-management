﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lift_Management.Model;

namespace Lift_Management.Repository
{
    interface IAddressRepository : IRepository<Address>
    {
        Address GetAddressById(Guid id);
    }
}
